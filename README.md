# WorkTime
Tiny helper which shows how long your computer has been unlocked. Get a rough idea how long you have been working today.

Sits at top of screen and show active hours and minutes.

![screenshot](worktime.png)

[Download](https://codeberg.org/CodeClaw/WorkTime/raw/branch/master/app/bin/Release/WorkTime.exe)

Note: Click between "Active:" and counter to drag window to the desired position.